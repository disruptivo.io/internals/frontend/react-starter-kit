import { mount } from 'navi';

import getPublicViews from '@routes/routesLoaders/publicRoutes';
import getPrivateViews from '@routes/routesLoaders/privateRoutes';

const getViews = async () => {
  const publicPages = await getPublicViews();
  const privatePages = await getPrivateViews();

  return {
    ...privatePages,
    ...publicPages,
  };
};

export default async () => mount(
  await getViews(),
);
