import getItemFromCookies from '@utils/getItemFromCookies';

export default () => getItemFromCookies('token');
