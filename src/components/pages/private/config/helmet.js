import React from 'react';

export default {
  title: 'Helmet with navi',
  head: (
    <>
      <meta
        name='description1'
        content='amazeballs1'
      />
      <meta
        name='description2'
        content='amazeballs2'
      />
    </>
  ),
  data: {
    name: 'Helmet body from config file',
  },
};