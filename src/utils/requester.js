import axios from 'axios';

class Requester {
  constructor() {
    this.axios = axios.create({
      baseURL: process.env.BASE_URL,
    });
  }

  get(
    url,
  ) {
    return this.axios(
      {
        url,
        method: 'GET',
      },
    );
  }

  post(
    url, data,
  ) {
    return this.axios(
      {
        url,
        data,
        method: 'POST',
      },
    );
  }
}

export default new Requester();
