import React, { createContext, useContext } from 'react';
import { useInterpret } from '@xstate/react';
import homeMachine from '@stateMachines/pages/home';
import loginMachine from '@stateMachines/pages/login';
import propTypes from 'prop-types';

export const StoreContext = createContext([]);
const StoreProvider = ({ children }) => {
  const homeService = useInterpret(homeMachine, { devTools: process.env.ENV === 'development' });
  const loginService = useInterpret(loginMachine, { devTools: process.env.ENV === 'development' });

  return (
    <StoreContext.Provider
      value={{ homeService, loginService }}
    >
      {children}
    </StoreContext.Provider>
  );
};

StoreProvider.propTypes = {
  children: propTypes.node.isRequired,
};

export default StoreProvider;
export const useStoreContext = () => useContext(
  StoreContext,
);