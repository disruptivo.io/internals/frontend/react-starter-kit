#### 0.6.1 (2021-12-04)

##### Documentation Changes

*  update readme file ([31ef966a](git+https://gitlab.com/luisbar/react-starter-kit/commit/31ef966a6c091824e6a783f9dba0d1d0904af6fd))

##### Code Style Changes

*  remove unused classes ([2e59962a](git+https://gitlab.com/luisbar/react-starter-kit/commit/2e59962addeadbc4db7bad7a81a215bdd1b6cca9))

### 0.6.0 (2021-09-16)

##### Continuous Integration

*  fix typo related to environment url ([7df8b053](git+https://gitlab.com/luisbar/react-starter-kit/commit/7df8b053c179843428e9e0e0df12743010aa84b0))

##### New Features

*  add paginator to components page ([119775fa](git+https://gitlab.com/luisbar/react-starter-kit/commit/119775fa9b7359b82f0c27734d5a10aefceeab68))
*  create paginator component ([9c4d21ed](git+https://gitlab.com/luisbar/react-starter-kit/commit/9c4d21ed2ffcae85befcb4a832175ced62f08694))

##### Bug Fixes

*  add dark style for the paginator ([94b635fa](git+https://gitlab.com/luisbar/react-starter-kit/commit/94b635fa8b2579c17850da2a2c5ecaea6410096f))
*  add missing strings of the paginator ([b4f9ed10](git+https://gitlab.com/luisbar/react-starter-kit/commit/b4f9ed1066dd03d4df67b1c3be4b3c7e7900a56d))

##### Code Style Changes

*  add a color to tailwind ([e818851a](git+https://gitlab.com/luisbar/react-starter-kit/commit/e818851ac4d60bcf146a080eda849b0aee335252))

### 0.5.0 (2021-09-16)

##### New Features

*  add dropdown component to main template ([bed9acfe](git+https://gitlab.com/luisbar/react-starter-kit/commit/bed9acfe17aab995ce8249b9f317e21beceeface))
*  add grid utils to tailwind ([d9c6143b](git+https://gitlab.com/luisbar/react-starter-kit/commit/d9c6143bf0bbde660e3139e05d332049ad751595))
*  create table component ([554caa1e](git+https://gitlab.com/luisbar/react-starter-kit/commit/554caa1e1a99991244a3c31b0585fd090e23c4bb))
*  add set cookie helper ([43b1c5d7](git+https://gitlab.com/luisbar/react-starter-kit/commit/43b1c5d73965f9e2235773670cc96f359e962843))
*  update not found and splash components ([934afe6a](git+https://gitlab.com/luisbar/react-starter-kit/commit/934afe6ad5540e29817b4139f7aa776244410af3))
*  add checkbox component to main template ([fcbf3ae9](git+https://gitlab.com/luisbar/react-starter-kit/commit/fcbf3ae9d845c76107e1ba79cd8ecc0888ebf4e7))

##### Bug Fixes

*  set the key in the value property of the option tag ([2e831660](git+https://gitlab.com/luisbar/react-starter-kit/commit/2e8316604562dff9a36b897397b9e4e3160a52d9))
*  change usemachine by useinterpret in order to fix a devtool problem ([e8d221c0](git+https://gitlab.com/luisbar/react-starter-kit/commit/e8d221c08e6d26f5703d303ed1e2e6b0e26a7aad))

##### Other Changes

*  fix eslint issues ([dffab240](git+https://gitlab.com/luisbar/react-starter-kit/commit/dffab240679d92b456b438cc6ccb926924e9cb78))

##### Code Style Changes

*  add dark style to table component ([f17ac2c3](git+https://gitlab.com/luisbar/react-starter-kit/commit/f17ac2c3d4799255c53b8faf8a15b4b66968a6da))
*  add dark colors to log out form and side menu components ([fb0dfab0](git+https://gitlab.com/luisbar/react-starter-kit/commit/fb0dfab0ed2e81e04b9470653da3ab6716326627))

##### Tests

*  add a flag to checkbox component in order to test it ([34dc8aa2](git+https://gitlab.com/luisbar/react-starter-kit/commit/34dc8aa274e3f348f6db581c0c18a766e9cf6be1))

### 0.4.0 (2021-09-14)

##### New Features

*  add markdown, helmet and components pages to side menu ([5f54d3d0](git+https://gitlab.com/luisbar/react-starter-kit/commit/5f54d3d06eece919a2f9e1a4089df054817486cc))
*  replace header and body template in home component by main template ([7803c96a](git+https://gitlab.com/luisbar/react-starter-kit/commit/7803c96a8f9f4be652f77f86ab43063ad23bb404))
*  add grid styles to tailwind ([d1fa0f42](git+https://gitlab.com/luisbar/react-starter-kit/commit/d1fa0f429151700a059eb3d4b0f71bb5048796cc))
*  add stateless transition lo login form in order to load session data ([d45dfa00](git+https://gitlab.com/luisbar/react-starter-kit/commit/d45dfa0007db012b2806b3a61b5f45ce8f9f862e))
*  create main template ([ca9f962e](git+https://gitlab.com/luisbar/react-starter-kit/commit/ca9f962ea010f77c748a7228796f3c563e6967cb))
*  create side menu component ([6c360ba3](git+https://gitlab.com/luisbar/react-starter-kit/commit/6c360ba3e6bf957dae2243587f7ce1c849de2306))
*  create logout form ([60c9d866](git+https://gitlab.com/luisbar/react-starter-kit/commit/60c9d8663743ea5bf5a16f7429c7f8ef1171d836))
*  create login page and become the home page a private page ([f05dd364](git+https://gitlab.com/luisbar/react-starter-kit/commit/f05dd364af2e8e55d044115794d817c8cbffada3))
*  create login form component ([1892a256](git+https://gitlab.com/luisbar/react-starter-kit/commit/1892a256ec41ebe736049040294eb92bf8b685a4))
*  enable cursor variant in tailwind ([aca6e982](git+https://gitlab.com/luisbar/react-starter-kit/commit/aca6e982ec245858580818bf53bac678fac110ed))
*  create centered template ([67795940](git+https://gitlab.com/luisbar/react-starter-kit/commit/67795940f42bfa76e6f19e831daeab0a1df5bf7c))
*  add form plugin to tailwind ([625bf5c3](git+https://gitlab.com/luisbar/react-starter-kit/commit/625bf5c351c71d1d8b660dd41de9670f834815e4))
*  create schema validator using joi ([e66c11f6](git+https://gitlab.com/luisbar/react-starter-kit/commit/e66c11f64ee88ddd54ffcc9a2f491a6c9d4db66f))
*  create dropdown component ([f3b30a2c](git+https://gitlab.com/luisbar/react-starter-kit/commit/f3b30a2cc7cc1a1e17288e3bfaac945671ab9c89))
*  create input component ([efc25c55](git+https://gitlab.com/luisbar/react-starter-kit/commit/efc25c55135767f95a26b0777b332f95550b5957))
*  create button component ([1c92f634](git+https://gitlab.com/luisbar/react-starter-kit/commit/1c92f63477eeb36687b7a6657e91c3dc86f6b62f))

##### Bug Fixes

*  fix issue in log out form related to xstate ([18c81687](git+https://gitlab.com/luisbar/react-starter-kit/commit/18c816879db624ac4887978c0e508f0033955dd8))
*  add semicolon to languages listener ([7b6d5615](git+https://gitlab.com/luisbar/react-starter-kit/commit/7b6d5615905f578506cdd5dc76dde21d117ec024))
*  pass missing properties to nested text component in button, input and dropdown components ([5a92896a](git+https://gitlab.com/luisbar/react-starter-kit/commit/5a92896a286c22e05fcae3cc1a1f6e5bc9c2362f))
*  add missing properties to input component ([d9de701d](git+https://gitlab.com/luisbar/react-starter-kit/commit/d9de701d14c4f2253b4b87d092301fa13ef07efb))

##### Other Changes

*  remove unused config file of wallet component ([6695e3b8](git+https://gitlab.com/luisbar/react-starter-kit/commit/6695e3b8a84966e9fd8684d8a1301cba7afe87d9))
*  remove wallet component ([0841f74e](git+https://gitlab.com/luisbar/react-starter-kit/commit/0841f74e06732629552d757708129194764aeca9))

##### Code Style Changes

*  remove unused property from login machine context ([cfdf72a4](git+https://gitlab.com/luisbar/react-starter-kit/commit/cfdf72a45d0f062da196ce86f079a03b97a35f98))
*  remove height and width from image component ([7838f4c8](git+https://gitlab.com/luisbar/react-starter-kit/commit/7838f4c8127ad2ef8553b54ae3ec9254ac52ee8c))

#### 0.3.2 (2021-09-13)

##### Continuous Integration

*  fix lint test job ([605e1104](git+https://gitlab.com/luisbar/react-starter-kit/commit/605e11044acff4525abb4e7f24b13480d2bd6271))
*  fix issue with config files ([bb2811b8](git+https://gitlab.com/luisbar/react-starter-kit/commit/bb2811b880bc4f2a5f566e4efe8c25db6066d4b8))

#### 0.3.1 (2021-09-13)

##### Bug Fixes

*  add missing env var to .env.example file ([539b3d40](git+https://gitlab.com/luisbar/react-starter-kit/commit/539b3d403635df0737a1ecbe030b8c3d97a97b3b))

### 0.3.0 (2021-09-13)

##### Build System / Dependencies

*  add css resolver to webpack ([32ddbad2](git+https://gitlab.com/luisbar/react-starter-kit/commit/32ddbad26d5909c833db6921721548d19b5dc1ce))

##### Continuous Integration

*  add gitlab pipeline for ci/cd ([2887e01f](git+https://gitlab.com/luisbar/react-starter-kit/commit/2887e01f8b8d262bfac3e23c73e87dca682aa388))

##### New Features

*  create axios instance using the base url provided in the .env file ([14c32ace](git+https://gitlab.com/luisbar/react-starter-kit/commit/14c32ace57c0eed9254462ffb166168390823048))
*  add env var for disabling devtools ([503590cf](git+https://gitlab.com/luisbar/react-starter-kit/commit/503590cfbe54428f3053ed7f4399d10e9de1ea4d))
*  add global store ([7bfb7208](git+https://gitlab.com/luisbar/react-starter-kit/commit/7bfb72089b0c4c06ab7bed9a14fae81bc1267b7b))
*  add dotenv to webpack in order to add env vars in build time ([d08122b7](git+https://gitlab.com/luisbar/react-starter-kit/commit/d08122b79826b151215b564edbd954993066d026))
*  create and add global alert component ([1c752341](git+https://gitlab.com/luisbar/react-starter-kit/commit/1c75234158b61f3addd6e57e9bb4a15e1ea207e2))
*  add colors to tailwind configuration ([8f4136b1](git+https://gitlab.com/luisbar/react-starter-kit/commit/8f4136b146564c83a7020890c278945e744f7ee3))
*  add internationalization provider component ([fa08214c](git+https://gitlab.com/luisbar/react-starter-kit/commit/fa08214cc37c1a79f891209b7528eaffe7b5f5e5))
*  add internationalization to text component of type md ([bdf9a9c2](git+https://gitlab.com/luisbar/react-starter-kit/commit/bdf9a9c29fe1b9506363a0e495e277bb624901b4))

##### Bug Fixes

*  clone array before poping in home and checkbox components ([7ea97aec](git+https://gitlab.com/luisbar/react-starter-kit/commit/7ea97aec803bac0441dcb023e1aa14a72835976c))
*  validate if classname props exist on globalspinner using optional chainning ([11e272d2](git+https://gitlab.com/luisbar/react-starter-kit/commit/11e272d230668ed384124929b2eb9d4c2f37ec04))
*  add right path for dotenv file ([d8c65c0e](git+https://gitlab.com/luisbar/react-starter-kit/commit/d8c65c0e79b2ddb57ca98e67ba2babba18fae3e0))

##### Other Changes

*  fix a comment ([22faba03](git+https://gitlab.com/luisbar/react-starter-kit/commit/22faba03a697a3674cf1e17e376bd58920ee3f6a))
*  enable react/require-default-props eslint rule ([60d5531f](git+https://gitlab.com/luisbar/react-starter-kit/commit/60d5531f1fe4ec462c4ff3e94305f4b7a97fdd09))
*  remove arrow-body-style eslint rule ([6b39bd17](git+https://gitlab.com/luisbar/react-starter-kit/commit/6b39bd176f76d7eae7c807f761974d592a3f79d6))
*  enable import/extensions eslint rule ([c87fa355](git+https://gitlab.com/luisbar/react-starter-kit/commit/c87fa3556f8cd3d4836f9084b671066d72c52917))
*  enable no-use-before-define eslint rule ([685a4a7b](git+https://gitlab.com/luisbar/react-starter-kit/commit/685a4a7b82c8af476e434df5b39ffe7ca12f3951))
*  enable semi eslint rule ([1bcd9049](git+https://gitlab.com/luisbar/react-starter-kit/commit/1bcd9049a6b3b114c3a07792e7776a9e8b570c6b))
*  enable react/prop-types eslint rule ([19d5117e](git+https://gitlab.com/luisbar/react-starter-kit/commit/19d5117e5a6763b85effdb2cd59a7c044f988573))
*  remove import/prefer-default-export eslint rule ([a2cc7a62](git+https://gitlab.com/luisbar/react-starter-kit/commit/a2cc7a62654cd9a1bb2e73b95f726d709614018d))
*  enable no-await-in-loop eslint rule and disable no-async-promise-executor eslint rule ([b4519698](git+https://gitlab.com/luisbar/react-starter-kit/commit/b45196982aa404de77a27111a00eb95eefee6f74))
*  enable no-plusplus eslint rule ([1eef77b9](git+https://gitlab.com/luisbar/react-starter-kit/commit/1eef77b93053002cec06019863a106f1ff576d30))
*  enable no-trailing-spaces eslint rule ([ded1429d](git+https://gitlab.com/luisbar/react-starter-kit/commit/ded1429d2e6862f148100425a432058119ff16d3))
*  remove comma-dangle eslint rule ([39b22f85](git+https://gitlab.com/luisbar/react-starter-kit/commit/39b22f85662a76f2b2d7d596bee39445ba559efb))
*  enable class-methods-use-this eslint rule ([b9a01d26](git+https://gitlab.com/luisbar/react-starter-kit/commit/b9a01d26070b9d5042cc8f392733358bbcb67914))
*  enable padded-blocks eslint rule ([0d70d267](git+https://gitlab.com/luisbar/react-starter-kit/commit/0d70d26709d2fe93acf2bfff3106bd38f65e0c53))
*  enable space-before-function-paren eslint rule ([cbcff5b9](git+https://gitlab.com/luisbar/react-starter-kit/commit/cbcff5b9c40ec63f004c03d0d0309c8619dc13d6))
*  enable no-alert and no-console eslint rules ([9d146f93](git+https://gitlab.com/luisbar/react-starter-kit/commit/9d146f93ef4c660fe07d3900e9e2f5c2c5f4fa87))
*  enable react/prefer-stateless-function eslint rule ([5ad01d76](git+https://gitlab.com/luisbar/react-starter-kit/commit/5ad01d767c5c1f9cd5110bc65ca7ef58663dede7))
*  enable react/jsx-one-expression-per-line eslint rule ([d1e2d54b](git+https://gitlab.com/luisbar/react-starter-kit/commit/d1e2d54b53c32daf7911d3445ee5663a44bd2a4b))
*  remove import/no-unresolved rule from eslint and install libraries for resolving alias ([c513f6ea](git+https://gitlab.com/luisbar/react-starter-kit/commit/c513f6ea84c3a4ebad51c7b46cd741db992f6ded))
*  enable jest on eslint ([061caa4d](git+https://gitlab.com/luisbar/react-starter-kit/commit/061caa4df3d1aaf7055f17945a0127ceac09e9ac))
*  fix eslint issue on index file ([2a53ed65](git+https://gitlab.com/luisbar/react-starter-kit/commit/2a53ed65b3db981aecab36c48904c722d27d36a6))
*  remove prettier ([4231c21d](git+https://gitlab.com/luisbar/react-starter-kit/commit/4231c21d52ece733bbe7916d6e45432bcbcbda5b))
*  fix linstaged configuration ([a5594a1b](git+https://gitlab.com/luisbar/react-starter-kit/commit/a5594a1bdec7901396d1128e87c81cc528a0bb4c))
* write for fixing eslint issues ([51ee10df](git+https://gitlab.com/luisbar/react-starter-kit/commit/51ee10dfac012de403a405ac205ca8951524972b))
* write for fixing linting issues ([e35f8543](git+https://gitlab.com/luisbar/react-starter-kit/commit/e35f8543d6a1d4f359675db6391e87455288dd12))
*  add two scripts to package.json for eslint ([e5344845](git+https://gitlab.com/luisbar/react-starter-kit/commit/e5344845aa646c7849c4d312fa5cffc9d0664137))
*  disable eol-last rule in eslint ([a844fb2f](git+https://gitlab.com/luisbar/react-starter-kit/commit/a844fb2fb5508b51c6f94669844c9407e91b1f1c))
* write script ([25b9c3bd](git+https://gitlab.com/luisbar/react-starter-kit/commit/25b9c3bd46551e9f2489ee64a2dea0d2bc630507))
*  update eslint configuration ([707ac33d](git+https://gitlab.com/luisbar/react-starter-kit/commit/707ac33d1a581de3851c269baae1341ffd60adb8))
*  add bundler folder to gitignore ([ae6e019f](git+https://gitlab.com/luisbar/react-starter-kit/commit/ae6e019f4496c78e729177ed3060af2f978aa3f8))
*  add jsconfig file in order to visual studio code resolves webpack alias ([0398edb8](git+https://gitlab.com/luisbar/react-starter-kit/commit/0398edb8c20c1fa225f5cb542a72bad5e679e214))
*  update node version on nvmrc file ([8f1659a8](git+https://gitlab.com/luisbar/react-starter-kit/commit/8f1659a8bb8cbce372a5d1346151ab5eca8488cb))

##### Refactors

*  change checkbox container ([d85726c3](git+https://gitlab.com/luisbar/react-starter-kit/commit/d85726c34b0be10fa748b1a0d59b26015bf98db9))
*  refactor home component, its machine and children ([5cb5faa1](git+https://gitlab.com/luisbar/react-starter-kit/commit/5cb5faa14762a28eef3efd8489c9f48bac0c7f17))
*  update webpack alias ([18113afc](git+https://gitlab.com/luisbar/react-starter-kit/commit/18113afc723dc2e072b3e3ac4a3c9211dc953949))
*  refactor global spinner component and its machine ([7c556953](git+https://gitlab.com/luisbar/react-starter-kit/commit/7c556953c2b16259f71c3b2521d4fc5a1a4e55f0))

##### Code Style Changes

*  add missing comma-dangle ([fd8cc297](git+https://gitlab.com/luisbar/react-starter-kit/commit/fd8cc297e627c4df9a0b38817e008647aa439475))
*  fix eslint issues on card component ([d5c2d2a6](git+https://gitlab.com/luisbar/react-starter-kit/commit/d5c2d2a6cd97f5c9586b2114e4b99a36917acb63))
*  fix eslint issues on language index and language listener script ([5e0c4091](git+https://gitlab.com/luisbar/react-starter-kit/commit/5e0c409100775970c7ca605b4a91c04083d21bd8))
*  fix eslint issues on home machine ([40f9e95d](git+https://gitlab.com/luisbar/react-starter-kit/commit/40f9e95dcc655a236678e1c79ee7b872b9a08f6e))
*  fix eslint issues on checkbox.test.js file ([1a6aa671](git+https://gitlab.com/luisbar/react-starter-kit/commit/1a6aa6712006211389568ac253c110632315dcac))
*  fix eslint issues on internatinalization index ([1ab41add](git+https://gitlab.com/luisbar/react-starter-kit/commit/1ab41add293069855eda0f64c9362020f4e30d84))
*  fix eslint issues on headerandbody component ([9c952237](git+https://gitlab.com/luisbar/react-starter-kit/commit/9c952237dc0e66ac732440a1b04567cbd739b123))
*  update react/jsx-filename-extension eslint rule ([64e11a5b](git+https://gitlab.com/luisbar/react-starter-kit/commit/64e11a5b799f9941e1343b1490d2e090b8832406))
*  fix eslint issues in home component ([52dc042b](git+https://gitlab.com/luisbar/react-starter-kit/commit/52dc042bd233d2a2d10c61deb83653f9b3e36a71))
*  remove unused react import ([ed9c00a4](git+https://gitlab.com/luisbar/react-starter-kit/commit/ed9c00a4b5e7aa98026f11bc72c674dcc59119b3))
*  fix eslint issues on text component ([6af285c9](git+https://gitlab.com/luisbar/react-starter-kit/commit/6af285c903212f6df549fbba184a1467b072c567))
*  fix eslint issues on image component ([a24fbc51](git+https://gitlab.com/luisbar/react-starter-kit/commit/a24fbc51219adae039455d52b568c2e3841b2444))
*  fix eslint issues on globalspinner component ([88246702](git+https://gitlab.com/luisbar/react-starter-kit/commit/882467026e291b6368a0df9a9e09caa884da3f56))
*  fix eslint issues on checkbox component ([2d3a05a0](git+https://gitlab.com/luisbar/react-starter-kit/commit/2d3a05a02c60c53c4161e441050081b2276e63da))
*  update react/forbid-prop-types eslint rule ([d75d3750](git+https://gitlab.com/luisbar/react-starter-kit/commit/d75d37508116bdf0215cf205d10374b6b5e4f8d0))
*  add react/jsx-max-props-per-line and react/jsx-first-prop-new-line eslint rules ([0733b69a](git+https://gitlab.com/luisbar/react-starter-kit/commit/0733b69ad70d98f6c12ed96fc60a5023a2c7d370))
*  update printwidth prettier rule ([0a4b035f](git+https://gitlab.com/luisbar/react-starter-kit/commit/0a4b035f4aa48607bdbe60e4d631aff75c7e3f9f))
*  remove max-len eslint rule ([b18abb55](git+https://gitlab.com/luisbar/react-starter-kit/commit/b18abb5562079a5917a831376a062ead91be69f8))
*  disable react/require-default-props eslint rule ([581e34a6](git+https://gitlab.com/luisbar/react-starter-kit/commit/581e34a6055b5b222e4c0716bda98aa6f8b1c497))
*  run eslint indent rule ([79d3d862](git+https://gitlab.com/luisbar/react-starter-kit/commit/79d3d86244943ee13fbae924c51e9089b1775b21))
*  run arrow-body-style rule ([86a6bf19](git+https://gitlab.com/luisbar/react-starter-kit/commit/86a6bf193fb7b7f87eaec3d605e9c2214fbf7e10))
*  update arrow-body-style rule ([5423aea3](git+https://gitlab.com/luisbar/react-starter-kit/commit/5423aea38927bedc5a761058a6c4e7bca1672f94))
*  run comma-dangle rule ([fe5ad236](git+https://gitlab.com/luisbar/react-starter-kit/commit/fe5ad2369632506218fc4b0b3d354a58d6b56832))
*  add space at the end and beginning of all arrays ([33fb7af5](git+https://gitlab.com/luisbar/react-starter-kit/commit/33fb7af51c4b0f73be450825cf82df72d2dacbc2))
*  validate curly on multiline blocks ([06b1dc7c](git+https://gitlab.com/luisbar/react-starter-kit/commit/06b1dc7c185799f77e9e1f89b7e37fd1958e91aa))
*  add arrow-body-style rule to eslint ([a23f1694](git+https://gitlab.com/luisbar/react-starter-kit/commit/a23f1694a97cee1fe02cd3f9810439c227a0d631))
*  change double quotes by single quotes on home and helmet component ([ec411361](git+https://gitlab.com/luisbar/react-starter-kit/commit/ec4113614189afec2c5f9187f60245526a64ee27))
*  validate the use of single quotes and template literals ([9b8d5d22](git+https://gitlab.com/luisbar/react-starter-kit/commit/9b8d5d22417496d32491a077160e7526ab3bf186))
*  validate single quotes on react component ([76ca8746](git+https://gitlab.com/luisbar/react-starter-kit/commit/76ca8746e266b1f8b2289b23a26ab5f04beb8d88))
*  remove object-curly-newline rule from eslint ([1349cca9](git+https://gitlab.com/luisbar/react-starter-kit/commit/1349cca9c10d43be33a7c1d3765d02a3439bf987))
*  remove function-paren-newline rule from eslint file ([691e0326](git+https://gitlab.com/luisbar/react-starter-kit/commit/691e03261296f20d5e6e24cf1c007d75a84a57d1))
*  update comma-dangle rule in eslint file ([a22448fb](git+https://gitlab.com/luisbar/react-starter-kit/commit/a22448fbf916d9073a5f5df191919a44aa075467))
*  remove ring from base style ([e6245df6](git+https://gitlab.com/luisbar/react-starter-kit/commit/e6245df61dfac7dc059b4d29b890575597b0b176))
*  change colors of the typhography ([7e7ca18b](git+https://gitlab.com/luisbar/react-starter-kit/commit/7e7ca18b7c7ba4d9bf4e91093d7d4d30d5e9462f))

### 0.2.0 (2021-08-02)

##### Build System / Dependencies

*  install test dependencies ([ed81590c](git+https://gitlab.com/luisbar/react-starter-kit/commit/ed81590ccf173d019d89eddb1bed4c6ed4a306b1))
*  move babel configuration from webpack file to .babelrc file ([46ee46e7](git+https://gitlab.com/luisbar/react-starter-kit/commit/46ee46e7a34ae813b6b2ba8f1d14a9b2cf26a5f2))
*  update package.lock.json file for node 16.5.0 ([0d845c03](git+https://gitlab.com/luisbar/react-starter-kit/commit/0d845c033b73345a765eaa7e3c063e1b391c633e))

##### New Features

*  configure jest ([388352c7](git+https://gitlab.com/luisbar/react-starter-kit/commit/388352c7225d776916065fb3da5d74576f5d84b8))

##### Bug Fixes

*  remove spawn checkbox from home component ([de750b98](git+https://gitlab.com/luisbar/react-starter-kit/commit/de750b986916cd4f0c84ae4225f61d95e8145dec))

##### Other Changes

*  update gitignore ([afb67b97](git+https://gitlab.com/luisbar/react-starter-kit/commit/afb67b9770fc8a52987901f9dce9117f8de54656))

##### Tests

*  write checkbox test ([f88390a3](git+https://gitlab.com/luisbar/react-starter-kit/commit/f88390a33dab0c078162d1aceddf7f8dab1d6de3))

### 0.1.0 (2021-03-11)

##### Build System / Dependencies

*  change release script ([a61ba751](git+https://gitlab.com/luisbar/react-starter-kit/commit/a61ba75131d7a3191d5152329fe51ea9ae73270d))
*  add scss loader ([904ee616](git+https://gitlab.com/luisbar/react-starter-kit/commit/904ee616a9fbfe3d683a8b8450dc7d3bed5b1409))
*  remove caret from package.json ([1b1b7e1e](git+https://gitlab.com/luisbar/react-starter-kit/commit/1b1b7e1e8f38052381790f452deec4a163cb4dd5))

##### Chores

*  update dependencies to remove vulnerabilities ([b7217a05](git+https://gitlab.com/luisbar/react-starter-kit/commit/b7217a05a3a71be4d03ff3067a0696739348e2a2))
*  update copy-webpack-plugin and webpack ([1314ddbe](git+https://gitlab.com/luisbar/react-starter-kit/commit/1314ddbec86d40b7384f82425711283ea7e336b1))
*  update node-sass and sass-loader ([dc20af58](git+https://gitlab.com/luisbar/react-starter-kit/commit/dc20af58f5a7ebcd0ebb2a29d06bd7c7356dd2f1))
*  run npm audit fix ([d0a4e7f5](git+https://gitlab.com/luisbar/react-starter-kit/commit/d0a4e7f52704b852b8e3c7a812b3b1a356d38d5d))
*  remove caret from package.json ([875a46b5](git+https://gitlab.com/luisbar/react-starter-kit/commit/875a46b56832ec9f308ed7e7d6c7f4b72777ae4d))
* **commitizen:**  [#33](git+https://gitlab.com/luisbar/react-starter-kit.git/pull/33) ([00891728](git+https://gitlab.com/luisbar/react-starter-kit/commit/00891728bf7b21eb9daf3acceddb61b2f48f0f2c))
* **package.json:**  [#34](git+https://gitlab.com/luisbar/react-starter-kit.git/pull/34) ([e452fa4a](git+https://gitlab.com/luisbar/react-starter-kit/commit/e452fa4a33218b634c19fa5dfedf95d51cd51b59))
* **npm modules:**  npm fix audit has been run ([75af9ea1](git+https://gitlab.com/luisbar/react-starter-kit/commit/75af9ea1770d955d1cad1c6240460bfff659f037))

##### New Features

*  add fast refresh ([9f083bbc](git+https://gitlab.com/luisbar/react-starter-kit/commit/9f083bbc60a5afac5056f2f3c2cad2916d670c79))
*  add remark for processing dynamic markdown content ([e6c7d053](git+https://gitlab.com/luisbar/react-starter-kit/commit/e6c7d05383cccedfea53062e29f5530b8f982b38))
*  add changelog generator ([263ce9f9](git+https://gitlab.com/luisbar/react-starter-kit/commit/263ce9f9bd8c300999e4f181051c78195a4b7bc9))
*  add mdx for processing markdown files ([e6548265](git+https://gitlab.com/luisbar/react-starter-kit/commit/e65482653ba03bf7fd0e8116512b5c6b99f17b22))
*  add immer ([806511d7](git+https://gitlab.com/luisbar/react-starter-kit/commit/806511d7d3b56feec07269ba523b7283b5e546e8))
*  add react helmet ([63e4fa19](git+https://gitlab.com/luisbar/react-starter-kit/commit/63e4fa197ee8d7c96976e5b882711d7918c5308d))
*  add xstate, tailwind, babel, prop-types, etc ([2fa3063b](git+https://gitlab.com/luisbar/react-starter-kit/commit/2fa3063b99bf2a9b82418a74242083d6b6bfb7ee))
*  dockerized the starter ([86c7e171](git+https://gitlab.com/luisbar/react-starter-kit/commit/86c7e171db9071864acb0b5513cddcfd5996cd8a))
*  add nvmrc and readme files ([ece8fad5](git+https://gitlab.com/luisbar/react-starter-kit/commit/ece8fad5c710144dbccc5e6f52ecdb9e5a6c4e45))
*  update commitizen config ([f7da37c0](git+https://gitlab.com/luisbar/react-starter-kit/commit/f7da37c045a63ec35ca20a64f7623c4d0824bb07))
* **commitlint:**  add commitlint ([17c24f8a](git+https://gitlab.com/luisbar/react-starter-kit/commit/17c24f8ae9fe1493e38433fbf6f4bbde73b61b63))
* **package.json/report.html/stats.json:**  execute webpack-analyzer report usin npm script ([f3363406](git+https://gitlab.com/luisbar/react-starter-kit/commit/f3363406831ce86c3e2ffc04b86270c7663ddd78))
* **package.json/webpack.dev.js:**  add webpack-bundle-analyzer plugin ([7becba63](git+https://gitlab.com/luisbar/react-starter-kit/commit/7becba633a13be7dc08010e4e0f90b205aa54da9))
* **favicon.png/logo.png:**  resize image ([18112d73](git+https://gitlab.com/luisbar/react-starter-kit/commit/18112d731fd606ad81fa07f3b38d41e01aa88834))
* **service worker:**  [#25](git+https://gitlab.com/luisbar/react-starter-kit.git/pull/25) ([bb9d5491](git+https://gitlab.com/luisbar/react-starter-kit/commit/bb9d549138ea6840ea85243d37eac65626b0e2e4))
* **utils/logger.ts state/middelwares.ts state/middlewares/logger.ts:**  add a logger and a logger mi ([8ab32430](git+https://gitlab.com/luisbar/react-starter-kit/commit/8ab3243068e631a55b319a79ca706db6585c17c6))
* **services/requester.ts:**  add axios mask ([f1d4d624](git+https://gitlab.com/luisbar/react-starter-kit/commit/f1d4d624395773ca6601038beb9146d73a08c353))
* **services.tsx:**  a little change ([f7c06208](git+https://gitlab.com/luisbar/react-starter-kit/commit/f7c062087a2592fe242321e8b016c8762158ce70))
* **assets/images Error.tsx:**  add animated logo ([5f368d42](git+https://gitlab.com/luisbar/react-starter-kit/commit/5f368d425563bc4cc1311fd51a7dc0bb2966a987))
* **private-folder:**  Add private page in order to test atomic design ([32085b07](git+https://gitlab.com/luisbar/react-starter-kit/commit/32085b070a3d3dfd51f842e92ed1f4eda8d095d1))
* **state/ui/spinner atoms/HorizontalSpinner.tsx:**  add spinner component ([83c5f723](git+https://gitlab.com/luisbar/react-starter-kit/commit/83c5f7236dd14eddba12a9a849aca90c78c52e2a))
* **templates atoms molecules organisms:**  use state manager ([3301ec9b](git+https://gitlab.com/luisbar/react-starter-kit/commit/3301ec9be2d7b6c18183953688602937f18059b0))
* **state utils components:**  add state global manager ([d4fba316](git+https://gitlab.com/luisbar/react-starter-kit/commit/d4fba31633053b17076e543a4f3d76a1cb992c4d))
* **strings internationalization.ts index.tsx:**  add react-intl ([ba408e03](git+https://gitlab.com/luisbar/react-starter-kit/commit/ba408e030d0452598fbe5505bb313888cf2e539a))
* **fonts images styles components:**  add some new features ([23eeb85d](git+https://gitlab.com/luisbar/react-starter-kit/commit/23eeb85dbd2983ddab73c5026b44571f7b6d00ea))
* **webpack.common.js styles Home.tsx Private.tsx:**  add sass-loader ([a9b9d1f8](git+https://gitlab.com/luisbar/react-starter-kit/commit/a9b9d1f85b27c3cdc9046c7ae74c34a864447e37))
* **eslintrc.js tsconfig.json Private.tsx Home.tsx:**  add css-modules ([76239505](git+https://gitlab.com/luisbar/react-starter-kit/commit/762395059a03f6b833ad9bda9aca492ce8922e8d))
* **webpack.common.js/favicon.png:**  Add react-starter-logo like favicon image ([a387d632](git+https://gitlab.com/luisbar/react-starter-kit/commit/a387d6321095d8c951042ff010898f1bdf870719))
* **package.json/settings.json:**
  *  Move eslint and babel-eslint to devDependencies section ([bf66c221](git+https://gitlab.com/luisbar/react-starter-kit/commit/bf66c221e266e5a2deb770276e270310d8a157d5))
  *  Add husky and integrate it with eslint ([313409e6](git+https://gitlab.com/luisbar/react-starter-kit/commit/313409e6c00287ad4e64df031c3ea99334c37271))
* **index.tsx/routes.tsx/Home.tsx/:**  Resolve conflicts with master ([05c82bf9](git+https://gitlab.com/luisbar/react-starter-kit/commit/05c82bf95f316337211b32381e73cd0d7ac6bbc7))
* **.eslintrc.js/index-HelloWorld.tsx:**  Update eslint rules and run it ([fc859fc6](git+https://gitlab.com/luisbar/react-starter-kit/commit/fc859fc6b803765c11c50d38ef776a85807c3cac))
* **.editorconfig/.eslitrc:**  Add editor configuration and add some new rules ([d158dcb6](git+https://gitlab.com/luisbar/react-starter-kit/commit/d158dcb6a235d8fe1dda0621c1fbe2a004664c14))
* **package.json:**  Update lint-staged ([abc7529a](git+https://gitlab.com/luisbar/react-starter-kit/commit/abc7529ae76d0911fa9c5e6490b05257ba00a425))
* **routes.tsx Home.tsx index.tsx package.json:**  add router ([9cc43203](git+https://gitlab.com/luisbar/react-starter-kit/commit/9cc4320318c0b57e1873c2e0c5cc17147b1a6e39))
* **package.json/prettier.config.js:**  Add prettier library in order to keeping good code practices ([433838ba](git+https://gitlab.com/luisbar/react-starter-kit/commit/433838ba0cab418229ee91a910085cb4de1d222f))
* **public src .gitignore package.json tsconfig.json webpack.config.js:**  add webpack, typescript an ([2812ce12](git+https://gitlab.com/luisbar/react-starter-kit/commit/2812ce123120d40b9f1d45c2e4679488e11d3d4c))
* **package.json .gitignore:**  initialize project ([98466d0a](git+https://gitlab.com/luisbar/react-starter-kit/commit/98466d0a99e7dd50b4da9f66d5534630bc70f2d7))

##### Bug Fixes

*  import images in a correct way ([d32a4ac6](git+https://gitlab.com/luisbar/react-starter-kit/commit/d32a4ac6afbe1f9dc51f5793b7e3aee75325a386))
* **.gitignore:**  remove reports folder ([c27cb848](git+https://gitlab.com/luisbar/react-starter-kit/commit/c27cb8483382ca348d2466e1aa219f288c1bddbf))
* **package.json:**  remove webpack-bundle-analyzer npm script ([6e0a7b4c](git+https://gitlab.com/luisbar/react-starter-kit/commit/6e0a7b4c9a0240828e6d22ec62c1b59099bfbb54))
* **declarations.d.ts:**  file removed from .gitignore ([80ce019a](git+https://gitlab.com/luisbar/react-starter-kit/commit/80ce019a8bd096174a1bf7afd8a4361c30116561))
* **typescript configuration:**  [#29](git+https://gitlab.com/luisbar/react-starter-kit.git/pull/29) ([d3dffc6b](git+https://gitlab.com/luisbar/react-starter-kit/commit/d3dffc6bd898e1480a7bc3dba2211c0dd8e34f79))
* **privateRoutes.ts publicRoutes.ts:**  eslint error has been fixed ([c3d418dd](git+https://gitlab.com/luisbar/react-starter-kit/commit/c3d418dd1122cc0ce0732d343b24629155b1346e))
* **webpack.prd.js:**  path of serviceWorker has been fixed ([d0a2b6df](git+https://gitlab.com/luisbar/react-starter-kit/commit/d0a2b6df3a6a162540a3b63424e48583a6fbc8f3))
* **service worker:**  workbox is being fetching from cloud ([e0028072](git+https://gitlab.com/luisbar/react-starter-kit/commit/e0028072a46155474cc59b4242cbd865f37cdf19))
* **typography:**  [#21](git+https://gitlab.com/luisbar/react-starter-kit.git/pull/21) ([5926255e](git+https://gitlab.com/luisbar/react-starter-kit/commit/5926255e3232df9896072f31ee45170aba2efeea))
* **fetchRickAndMortyData/action.ts:**  remove second parameter dispatch method ([97093dbd](git+https://gitlab.com/luisbar/react-starter-kit/commit/97093dbdab054c3e0901b2f36d87905fe461cd83))
* **many:**  merge master with feature/logger branch ([7485aa5e](git+https://gitlab.com/luisbar/react-starter-kit/commit/7485aa5ec6c556efd87479ee920df3cd70e33dd9))
* **FormattedMessage:**  add defaultMessage to formattedMessage component ([49f95b8d](git+https://gitlab.com/luisbar/react-starter-kit/commit/49f95b8dab612d738f308a10759a3f14315c62bd))
* **types:**  add types ([b2570ad2](git+https://gitlab.com/luisbar/react-starter-kit/commit/b2570ad211326f716733c233c26d0be6ed4d52f8))
* **eslintrc.js package.json:**  fix eslint rules ([a57226aa](git+https://gitlab.com/luisbar/react-starter-kit/commit/a57226aae6ee32736ab80ab223cd7a16cf5367c7))
* **routes.tsx:**  fix merge conflict ([4290c95a](git+https://gitlab.com/luisbar/react-starter-kit/commit/4290c95ab0f355e0923bef9f8b69ef8b2e692862))
* **watcher script:**  add watcher script ([fc2d0b00](git+https://gitlab.com/luisbar/react-starter-kit/commit/fc2d0b00694f02e7b2a5828f5b0330e401b619b5))
* **webpack:**  fix webpack configuration ([4d9ac9b2](git+https://gitlab.com/luisbar/react-starter-kit/commit/4d9ac9b2f26523ae34ecdae54d68db2403b52c25))

##### Other Changes

*  remove typography ([7709e417](git+https://gitlab.com/luisbar/react-starter-kit/commit/7709e4175c28dadc180754c9388901a14c1a62ea))
* luisbar/react-starter-kit ([b7e60bb0](git+https://gitlab.com/luisbar/react-starter-kit/commit/b7e60bb0ea9b89d2d85c33d93e90983fe2096f76))
* luisbar/react-starter-kit ([7ff141dc](git+https://gitlab.com/luisbar/react-starter-kit/commit/7ff141dc778cca0b8fd8faf96c2a24bc395f4b73))
* luisbar/react-starter-kit ([ac4a37c2](git+https://gitlab.com/luisbar/react-starter-kit/commit/ac4a37c2784f83ee49a078a707a7084c2749c61f))
* luisbar/react-starter-kit ([8dc75ac2](git+https://gitlab.com/luisbar/react-starter-kit/commit/8dc75ac2b3add2846110aac1f63af8e18920c737))
* gilsantosjulian/react-starter-kit ([f52de6aa](git+https://gitlab.com/luisbar/react-starter-kit/commit/f52de6aaf1dad8a9a051788d4c67c3c1f79fb310))
* **package-lock.json:**  merge due to conflict ([5ae28fbc](git+https://gitlab.com/luisbar/react-starter-kit/commit/5ae28fbc7d59eda4104dcbb380cd7bc6ec506f7c))

##### Refactors

* **package.json/webpack.common.js/webpack.dev.js:**  refctor npm bundle-report command using web ([2e7aa6ba](git+https://gitlab.com/luisbar/react-starter-kit/commit/2e7aa6bac020b1d29de006ac152aa2c8724030b2))
* **configuration files of some libraries:**  [#36](git+https://gitlab.com/luisbar/react-starter-kit.git/pull/36) ([ac7cb98a](git+https://gitlab.com/luisbar/react-starter-kit/commit/ac7cb98a1aa0435a4d1db112a025ccdf94deb13d))
* **serviceWorker:**  serviceWorker has been moved to src folder ([4235eb41](git+https://gitlab.com/luisbar/react-starter-kit/commit/4235eb41fab78bb62c7a1b156ccfbcdfe5e1536e))
* **folder names and files location:**  [#22](git+https://gitlab.com/luisbar/react-starter-kit.git/pull/22) ([a9e30ae7](git+https://gitlab.com/luisbar/react-starter-kit/commit/a9e30ae7670ef5b2a410069e131b733658072c2f))

##### Code Style Changes

*  improve organization of scss files ([5e9b2f4d](git+https://gitlab.com/luisbar/react-starter-kit/commit/5e9b2f4d73a5ab24d9585d40213baa4bfa1978ea))

