import React, { useEffect } from 'react';
import Main from '@templates/Main';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useStoreContext } from '@root/stateMachines/Store';
import Cards from '@organisms/Cards';
import { useActor } from '@xstate/react';

const Home = () => {
  const { homeService } = useStoreContext();
  const [ homeState, send ] = useActor(homeService);
  const globalSpinnerContext = useGlobalSpinnerContext();
  const toggleSpinner = [ ...globalSpinnerContext ].pop();

  useEffect(() => {
    send({
      type: 'LOAD',
    });
  }, []);

  useEffect(() => {
    if (homeState.matches('loading')) toggleSpinner({ type: 'ENABLE' });
    if (homeState.matches('loaded')) toggleSpinner({ type: 'DISABLE' });
  }, [ homeState.value ]);

  return (
    <Main>
      <Cards
        className='row-start-2 col-start-1 col-end-11'
        cards={homeState.context.features}
      />
    </Main>
  );
};

export default Home;
