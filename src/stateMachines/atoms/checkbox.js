import { createMachine, sendParent } from 'xstate';

export const machineDefinition = {
  id: 'checkbox',
  initial: 'unchecked',
  states: {
    unchecked: {
      on: {
        TOGGLE: [
          {
            actions: [ 'sendParent' ],
            target: 'checked',
          },
        ],
      },
    },
    checked: {
      on: {
        TOGGLE: [
          {
            actions: [ 'sendParent' ],
            target: 'unchecked',
          },
        ],
      },
    },
  },
};

const machineOptions = {
  actions: {
    sendParent: sendParent((context, event) => event.data),
  },
};

export default createMachine(machineDefinition, machineOptions);