# 😕 What is this?

A react boilerplate with the following features:

- Watcher which create routes and load new languages
- Tailwind ready to use
- It uses [atomic design structure](https://bradfrost.com/blog/post/atomic-web-design/)
- Internationalization (i18n)
- XState as global state manager
- Schema validator with Joi
- A basic service worker using Workbox
- Tools for standarized the commit messages:
    - commitizen
    - cz-customizable
- Tools for writing a clean code:
    - eslint
    - prettier
    - husky
    - lint-staged
- Env vars loaded by Webpack
- Alias defined by Webpack
- Code splitting by Webpack
- Changelog generator
- Gitlab CI/CD pipeline
- Dockerized
- Jest html reporters
- Markdown

# 🚀 How to install?
### Normal way

1. Install nvm

    Instructions [here](https://github.com/nvm-sh/nvm#installing-and-updating)

2. Clone the repo
```bash
git clone git@gitlab.com:luisbar/react-starter-kit.git
```

3. Enter to the folder
```bash
cd react-starter-kit
```

4. Install and choose version node required by the project
```bash
nvm install && nvm use
```

5. Install dependencies
```bash
npm i
```

6. Create `.env.development` file taking as reference the `.env.example`

7. Run the project
```bash
npm start
```
### Using docker

1. Clone the project
```bash
git clone git@gitlab.com:luisbar/react-starter-kit.git
```

2. Create `.env.development` file taking as reference the `.env.example`

3. Create the image
```bash
docker-compose build
```

4. Run the container
```bash
docker-compose up -d
```

# 🧐 What's inside?

    .
    ├── bundler
    │   ├── webpack.common.js
    │   ├── webpack.dev.js
    │   └── webpack.prod.js
    ├── public
    │   ├── favicon.png
    │   └── index.html
    ├── scripts
    │   └── watcher
    ├── src
    │   ├── assets
    │   ├── components
    │   ├── internationalization
    │   ├── routes
    │   ├── stateMachines
    │   ├── utils
    │   ├── index.js
    │   ├── indexWithServiceWorker.js
    │   └── serviceWorker.js
    ├── CHANGELOG.md
    ├── Dockerfile
    ├── Readme.md
    ├── docker-compose.yaml
    ├── jest.config.js
    ├── jest.dom.js
    ├── jest.utils.js
    ├── jsconfig.json
    ├── package-lock.json
    ├── package.json
    ├── postcss.config.js
    └── tailwind.config.js

- **bundler:** webpack configuration
- **scripts:** watcher which watch:
    - `src/components/pages/private`
        - If you insert a file in this folder with `tsx` or `md` extension, a private route is created, and this route is guarded with the `isAuthenticated.js` script which is loacated in `src/routes/guards`
        - Additionally a config file is created for each page, through it you can pass some parameters because it uses `react-navi` as router
    - `src/components/pages/public`
        - If you insert a file in this folder with `tsx` or `md` extension, a public route is created
        - Additionally a config file is created for each page, through it you can pass some parameters because it uses `react-navi` as router
    - `src/components/internationalization/languages`
        - If you create a `json` file here a new language is loaded into react-i18n
- **assets:** images and styles files
- **components:** graphic components organized by using [atomic design structure](https://bradfrost.com/blog/post/atomic-web-design/)
- **internationalization:** internationalization configuration, you just have to add new languages into `languages` folder and populate it
- **routes:** it containts all the router configuration, here you just have to update the `isAuthenticated.js` file
- **state machines:** state machine definitions organized by using [atomic design structure](https://bradfrost.com/blog/post/atomic-web-design/) and the `StoreProvider`, which returns the global state
- **utils:** utils scripts such as `schemaValidator`, `requester`, etc